#[cfg(test)]
mod tests_after {
    use s_test_fixture::after;
    use std::fs::{remove_file, File, OpenOptions};
    use std::io::prelude::*;

    #[test]
    #[after(validate_simple_test(1))]
    fn simple_test_after() {
        File::create(format!("{}.txt", 1)).unwrap();
        something(1);
    }

    #[test]
    #[after(validate_branch_test(2))]
    fn branch_test_after() {
        File::create(format!("{}.txt", 2)).unwrap();
        something(2);
        if 1 == 0 {
            something(2);
            ()
        } else if 1 == 2 {
            something(2);
            return ();
        } else {
            something(2);
            ()
        }
    }

    #[test]
    #[after(validate_returning_a_result(3))]
    fn test_returning_a_result_after() -> Result<(), ()> {
        File::create(format!("{}.txt", 3)).unwrap();
        something(3);
        something(3);
        something(3);
        something(3);
        Ok(())
    }

    #[test]
    #[after(validate_simple_test(4))]
    #[allow(unused_attributes)]
    #[should_panic]
    fn test_panic_after() {
        File::create(format!("{}.txt", 4)).unwrap();
        something(4);
        panic!("I panic")
    }

    fn something(n: i32) {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(format!("{}.txt", n))
            .unwrap();

        writeln!(file, "{}", MSG).unwrap();
    }

    fn read_and_delete_test_file(n: i32) -> String {
        let mut file = File::open(format!("{}.txt", n)).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        remove_file(format!("{}.txt", n)).unwrap();
        contents
    }

    fn validate_simple_test(n: i32) {
        let statement = format!(
            "{msg}\n{msg_fixture}\n",
            msg = MSG,
            msg_fixture = MSG_FIXTURE
        );
        function_to_run(n, statement);
    }

    fn validate_branch_test(n: i32) {
        let statement = format!(
            "{msg}\n{msg}\n{msg_fixture}\n",
            msg = MSG,
            msg_fixture = MSG_FIXTURE
        );
        function_to_run(n, statement);
    }

    fn validate_returning_a_result(n: i32) {
        let statement = format!(
            "{msg}\n{msg}\n{msg}\n{msg}\n{msg_fixture}\n",
            msg = MSG,
            msg_fixture = MSG_FIXTURE
        );
        function_to_run(n, statement);
    }

    fn function_to_run(n: i32, statement: String) {
        let msg = format!("{}\n", MSG_FIXTURE);

        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(format!("{}.txt", n))
            .unwrap();

        write!(file, "{}", msg).unwrap();

        assert_eq!(statement, read_and_delete_test_file(n));
    }

    const MSG: &'static str = "something was done";
    const MSG_FIXTURE: &'static str = "after fixture";
}

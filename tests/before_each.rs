use s_test_fixture::before_each;
#[cfg(test)]
#[before_each(function_to_run(22))]
mod tests_before_each {
    use std::fs::{remove_file, File, OpenOptions};
    use std::io::prelude::*;

    #[test]
    fn simple_test_before_each() {
        something();
        assert_eq!(
            format!(
                "{msg_fixture}\n{msg}\n",
                msg = MSG,
                msg_fixture = MSG_FIXTURE
            ),
            read_and_delete_test_file(22)
        );
    }

    #[test]
    fn branch_test_before_each() {
        something();
        if 1 == 0 {
            something();
            ()
        } else if 1 == 2 {
            something();
            return ();
        } else {
            something();
            assert_eq!(
                format!(
                    "{msg_fixture}\n{msg}\n{msg}\n",
                    msg = MSG,
                    msg_fixture = MSG_FIXTURE
                ),
                read_and_delete_test_file(22)
            );
            ()
        }
    }

    #[test]
    fn test_returning_a_result_before_each() -> Result<(), ()> {
        something();
        something();
        something();
        something();
        assert_eq!(
            format!(
                "{msg_fixture}\n{msg}\n{msg}\n{msg}\n{msg}\n",
                msg = MSG,
                msg_fixture = MSG_FIXTURE
            ),
            read_and_delete_test_file(22)
        );
        Ok(())
    }

    fn something() {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(format!("{}.txt", 22))
            .unwrap();

        writeln!(file, "{}", MSG).unwrap();
    }

    fn read_and_delete_test_file(n: i32) -> String {
        let mut file = File::open(format!("{}.txt", n)).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        remove_file(format!("{}.txt", n)).unwrap();
        contents
    }

    fn function_to_run(n: i32) {
        let mut file = File::create(format!("{}.txt", n)).unwrap();
        let msg = format!("{}\n", MSG_FIXTURE);
        file.write_all(msg.as_bytes()).unwrap();
    }

    const MSG: &'static str = "something was done";
    const MSG_FIXTURE: &'static str = "before fixture";
}

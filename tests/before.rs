#[cfg(test)]
mod tests_before {
    use s_test_fixture::before;
    use std::fs::{remove_file, File, OpenOptions};
    use std::io::prelude::*;

    #[test]
    #[before(function_to_run(1))]
    fn simple_test_before() {
        something(1);
        assert_eq!(
            format!("{msg_fixture}\n{msg}\n", msg = MSG, msg_fixture = MSG_FIXTURE),
            read_and_delete_test_file(1)
        );
    }

    #[test]
    #[before(function_to_run(2))]
    fn branch_test_before() {
        something(2);
        if 1 == 0 {
            something(2);
            ()
        } else if 1 == 2 {
            something(2);
            return ();
        } else {
            something(2);
            assert_eq!(
                format!(
                    "{msg_fixture}\n{msg}\n{msg}\n",
                    msg = MSG,
                    msg_fixture = MSG_FIXTURE
                ),
                read_and_delete_test_file(2)
            );
            ()
        }
    }

    #[test]
    #[before(function_to_run(3))]
    fn test_returning_a_result_before()->Result<(),()> {
        something(3);
        something(3);
        something(3);
        something(3);
        assert_eq!(
            format!("{msg_fixture}\n{msg}\n{msg}\n{msg}\n{msg}\n", msg = MSG, msg_fixture = MSG_FIXTURE),
            read_and_delete_test_file(3)
        );
        Ok(())
    }

    fn something(n: i32) {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(format!("{}.txt", n))
            .unwrap();

        writeln!(file, "{}", MSG).unwrap();
    }

    fn read_and_delete_test_file(n: i32) -> String {
        let mut file = File::open(format!("{}.txt", n)).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        remove_file(format!("{}.txt", n)).unwrap();
        contents
    }

    fn function_to_run(n: i32) {
        let mut file = File::create(format!("{}.txt", n)).unwrap();
        let msg = format!("{}\n", MSG_FIXTURE);
        file.write_all(msg.as_bytes()).unwrap();
    }

    const MSG: &'static str = "something was done";
    const MSG_FIXTURE: &'static str = "before fixture";
}

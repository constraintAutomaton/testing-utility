use s_test_fixture::after_each;

#[cfg(test)]
#[after_each(validate())]
mod tests_after_each {
    use std::fs::{remove_file, File, OpenOptions};
    use std::io::prelude::*;

    #[test]
    fn simple_test_after_each() {
        File::create(format!("{}.txt", 44)).unwrap();
        something();
        assert_eq!(format!("{msg}\n", msg = MSG), read_test_file());
    }

    #[test]
    fn branch_test_after_each_else() {
        File::create(format!("{}.txt", 44)).unwrap();
        something();
        if 1 == 0 {
            something();
            ()
        } else if 1 == 2 {
            something();
            return ();
        } else {
            something();
            assert_eq!(format!("{msg}\n{msg}\n", msg = MSG), read_test_file());
            ()
        }
    }

    #[test]
    fn test_returning_a_result_after_each() -> Result<(), ()> {
        File::create(format!("{}.txt", 44)).unwrap();
        something();
        something();
        something();
        something();
        assert_eq!(format!("{msg}\n{msg}\n{msg}\n{msg}\n", msg = MSG), read_test_file());
        Ok(())
    }

    #[test]
    fn branch_test_after_each_if() {
        File::create(format!("{}.txt", 44)).unwrap();
        something();
        if 1 == 1 {
            something();
            assert_eq!(format!("{msg}\n{msg}\n", msg = MSG), read_test_file());
            ()
        } else if 1 == 2 {
            something();
            return ();
        } else {
            something();
            ()
        }
    }

    #[test]
    fn branch_test_after_each_else_if() {
        File::create(format!("{}.txt", 44)).unwrap();
        something();
        if 1 == 0 {
            something();
            assert_eq!(format!("{msg}\n{msg}\n", msg = MSG), read_test_file());
            ()
        } else if 1 == 1 {
            something();
            assert_eq!(format!("{msg}\n{msg}\n", msg = MSG), read_test_file());
            return ();
        } else {
            something();
            assert_eq!(format!("{msg}\n{msg}\n", msg = MSG), read_test_file());
            ()
        }
    }
    
    fn something() {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(format!("{}.txt", 44))
            .unwrap();

        writeln!(file, "{}", MSG).unwrap();
    }

    fn read_test_file() -> String {
        let mut file = File::open(format!("{}.txt", 44)).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        contents
    }

    fn validate() {
        remove_file(format!("{}.txt", 44)).unwrap();
        match File::open(format!("{}.txt", 44)) {
            Err(_) => (),
            Ok(_) => panic!("the file should be deleted"),
        }
    }

    const MSG: &'static str = "something was done";
}

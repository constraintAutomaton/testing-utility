use s_test_fixture::{after, after_each, before, before_each};

#[cfg(test)]
#[after_each(validate())]
#[before_each(function_to_run(11))]
mod tests_after_before_each {
    use std::fs::{remove_file, File, OpenOptions};
    use std::io::prelude::*;

    #[test]
    fn simple_test_after_each() {
        something();
        assert_eq!(
            format!(
                "{msg_fixture}\n{msg}\n",
                msg = MSG,
                msg_fixture = MSG_FIXTURE
            ),
            read_test_file()
        );
    }
    fn something() {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(format!("{}.txt", 11))
            .unwrap();

        writeln!(file, "{}", MSG).unwrap();
    }

    fn read_test_file() -> String {
        let mut file = File::open(format!("{}.txt", 11)).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        contents
    }

    fn validate() {
        remove_file(format!("{}.txt", 11)).unwrap();
        match File::open(format!("{}.txt", 11)) {
            Err(_) => (),
            Ok(_) => panic!("the file should be deleted"),
        }
    }
    fn function_to_run(n: i32) {
        let mut file = File::create(format!("{}.txt", n)).unwrap();
        let msg = format!("{}\n", MSG_FIXTURE);
        file.write_all(msg.as_bytes()).unwrap();
    }
    const MSG: &'static str = "something was done";
    const MSG_FIXTURE: &'static str = "before fixture";
}

#[cfg(test)]
mod tests_after_before {
    use super::*;
    use std::fs::{remove_file, File, OpenOptions};
    use std::io::prelude::*;

    #[test]
    #[before(function_to_run(77))]
    #[after(validate())]

    fn simple_test_after_each() {
        something();
        assert_eq!(
            format!(
                "{msg_fixture}\n{msg}\n",
                msg = MSG,
                msg_fixture = MSG_FIXTURE
            ),
            read_test_file()
        );
    }
    fn something() {
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(format!("{}.txt", 77))
            .unwrap();

        writeln!(file, "{}", MSG).unwrap();
    }

    fn read_test_file() -> String {
        let mut file = File::open(format!("{}.txt", 77)).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        contents
    }

    fn validate() {
        remove_file(format!("{}.txt", 77)).unwrap();
        match File::open(format!("{}.txt", 77)) {
            Err(_) => (),
            Ok(_) => panic!("the file should be deleted"),
        }
    }
    fn function_to_run(n: i32) {
        let mut file = File::create(format!("{}.txt", n)).unwrap();
        let msg = format!("{}\n", MSG_FIXTURE);
        file.write_all(msg.as_bytes()).unwrap();
    }
    const MSG: &'static str = "something was done";
    const MSG_FIXTURE: &'static str = "before fixture";
}

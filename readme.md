# s_test_fixture

s_test_fixture or simple test fixture is a macro library to implement test fixtures with no hassle.


## Usage
There are four macros: `before`, `after`, `before_each`, `after_each`. The function pass as arguments must return `()`.

`before` and `after` are simply added before the test function.

`before_each` and `after_each` behave in a similar manner and stack with `before` and `after` macro. It has to be noted that the scope of the function pass as argument matter, it has to be visible inside the test module.

### before
```rust
#[cfg(test)]
mod tests {
    use s_fixture::before;
    #[test]
    #[before(function_to_run(2))]
    fn test() {
        let a = 2;
        something();
        assert_eq!(a,2);
        println!("ending");

    }
    
    fn something(){}

    fn function_to_run(i:i32){
        println!("I did {} thing(s)",i);
    }
}
```
will return 

```
I did 2 thing(s)
ending
```
### after
```rust
#[cfg(test)]
mod tests {
    use s_fixture::after;
    #[test]
    #[after(function_to_run(66))]
    fn test() -> Result<(), ()> {
        println!("before");
        let i = -55;
        something();
        if i == 0 {
            something();
            Ok(())
        } else if i == 2 {
            something();
            return Ok(());
        } else {
            panic!("oh no!");
            Ok(())
        }
    }
    
    fn something(){}

    fn function_to_run(i:i32){
        println!("I did {} thing(s)",i);
    }
}

```
will return
```
before
{panic statement}
I did 66 thing(s)
```
### before_each
```rust
use s_fixture::{before_each,after, before};

#[cfg(test)]
#[before_each(function_to_run(78))]
mod tests {
    use super::*;
    #[test]
    #[before(function_to_run(2))]
    fn test() {
        let a = 2;
        something();
        assert_eq!(a,2);
        println!("ending");

    }
    
    fn something(){}

    fn function_to_run(i:i32){
        println!("I did {} thing(s)",i);
    }
}
```
will return
```
I did 2 thing(s)
I did 78 thing(s)
ending
```
## Test

run 
`cargo test -- --test-threads 1`